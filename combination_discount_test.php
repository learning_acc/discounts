<?php

require __DIR__ . '/vendor/autoload.php';

use Discount\CombinationDiscount;
use Collection\ProductCollection;

$a  = new Product('a', 100);
$b  = new Product('b', 300);
$c  = new Product('c', 200);
$notUserProducts = new ProductCollection([$a, $b, $c]);
assert(3 === $notUserProducts->count());

$cd = new CombinationDiscount(['a', 'b'], 0.5);

// Ссылка применилась к двум товарам
assert(2 === $cd->applyDiscount($notUserProducts)->getMatchedProducts()->count());

// Изначальная коллекция осталась нетронутой
assert(3 === $notUserProducts->count());
