<?php

require __DIR__ . '/vendor/autoload.php';

use Discount\CountDiscount;
use Discount\CombinationDiscount;
use Discount\CombinationOneOfDiscount;
use Collection\DiscountCollection;
use Collection\ProductCollection;

function equal($a, $b, $e = 0.0001) {
    if ($a === $b) {
        return true;
    }

    return abs($a - $b) < $e;
}

$a  = new Product('a', 100);
$a2 = new Product('a', 100);
$b  = new Product('b', 300);
$c  = new Product('c', 200);
$d  = new Product('d', 200);
$e  = new Product('e', 100);
$c2 = new Product('c', 100);

// Discount\CombinationDiscount
$cd = new CombinationDiscount(['a', 'b'], 0.5);
$c  = new Calculator();
assert(equal(200, $c->calculateTotalPrice(new DiscountCollection([$cd]), new ProductCollection([
        new Product('a', 100),
        new Product('a', 100),
    ]))));
assert(equal(100, $c->calculateTotalPrice(new DiscountCollection([$cd]), new ProductCollection([
        new Product('a', 100),
        new Product('b', 100),
    ]))));

$discountCollection = new DiscountCollection([
    new CombinationDiscount(['a', 'b', 'c'], 0.2),
    new CombinationDiscount(['a', 'b', 'c', 'd'], 0.8),
]);
$abcdCollection = new ProductCollection([
    new Product('a', 100),
    new Product('b', 100),
    new Product('c', 100),
    new Product('d', 100),
]);
assert(equal(100 + 300 - 300 * 0.2, $c->calculateTotalPrice($discountCollection, $abcdCollection)));

$discountCollection = new DiscountCollection([
    new CombinationDiscount(['a', 'b', 'c', 'd'], 0.8),
    new CombinationDiscount(['a', 'b', 'c'], 0.2),
]);
assert(equal(400 - 400 * 0.8, $c->calculateTotalPrice($discountCollection, $abcdCollection)));

// Discount\CombinationOneOfDiscount
$cod = new CombinationOneOfDiscount('a', ['b', 'c', 'd'], 0.5);
assert(equal(200 + 100, $c->calculateTotalPrice(new DiscountCollection([$cod]), $abcdCollection)));

// CountDiscount
$cd = new CountDiscount([
    1 => 0.3,
    2 => 0.4,
    3 => 0.5,
], ['a', 'b']);
assert(equal(200 - 200 * 0.4 + 200, $c->calculateTotalPrice(new DiscountCollection([$cd]), $abcdCollection)));

$cd = [2 => 0.5];
assert(equal(200, $c->calculateTotalPrice(new DiscountCollection([new CountDiscount($cd)]), $abcdCollection)));

// Main test
$discountCollection = new DiscountCollection([
    new CombinationDiscount(['a', 'b'], 0.1),
    new CombinationDiscount(['d', 'e'], 0.05),
    new CombinationDiscount(['f', 'e', 'g'], 0.05),
    new CombinationOneOfDiscount('a', ['k', 'j', 'm'], 0.05),
    new CountDiscount(
        [
            3 => 0.05,
            4 => 0.01,
            5 => 0.02
        ], ['a', 'c']
    ),
]);

$productCollection = new ProductCollection([
    new Product('a', 100),
    new Product('b', 100),
    new Product('c', 100),
    new Product('d', 100),
    new Product('e', 100),
    new Product('f', 100),
    new Product('g', 100),
    new Product('h', 100),
    new Product('i', 100),
    new Product('j', 100),
    new Product('a', 100),
]);

$answer = (200 - 200 * 0.1) + (200 - 200 * 0.05) + (200 - 200 * 0.05) + (400 - 400 * 0.01) + 100;
assert(equal($answer, $c->calculateTotalPrice($discountCollection, $productCollection)));
