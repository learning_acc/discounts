<?php

use Collection\ProductCollection;

class DiscountResult
{
    /**
     * @var ProductCollection
     */
    private $matchedProducts;

    /**
     * @var float|int
     */
    private $priceWithDiscount;

    public function __construct(ProductCollection $matchedProducts, $priceWithDiscount)
    {
        if ($priceWithDiscount < 0) {
            throw new \LogicException("Argument must be a positive, given {$priceWithDiscount}");
        }

        $this->priceWithDiscount = $priceWithDiscount;
        $this->matchedProducts = $matchedProducts;
    }

    /**
     * @return self
     */
    public static function makeEmpty()
    {
        return new self(new ProductCollection(), 0);
    }

    /**
     * @return ProductCollection
     */
    public function getMatchedProducts()
    {
        return $this->matchedProducts;
    }

    /**
     * @return float|int
     */
    public function getPriceWithDiscount()
    {
        return $this->priceWithDiscount;
    }
}