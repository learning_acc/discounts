<?php

namespace Collection;

use Discount\DiscountInterface;

class DiscountCollection implements \IteratorAggregate
{
    /**
     * @var DiscountInterface[]
     */
    private $discounts;

    public function __construct(array $discounts = [])
    {
        $this->discounts = [];
        foreach ($discounts as $discount) {
            $this->addDiscount($discount);
        }
    }

    public function addDiscount(DiscountInterface $discount)
    {
        $this->discounts[] = $discount;
    }

    /**
     * @return \ArrayIterator|DiscountInterface[]
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->discounts);
    }
}