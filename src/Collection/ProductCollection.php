<?php

namespace Collection;

use Product;

class ProductCollection implements \IteratorAggregate
{
    /**
     * @var \SplObjectStorage|Product[]
     */
    private $products;

    /**
     * @param Product[] $products
     */
    public function __construct(array $products = [])
    {
        $this->products = new \SplObjectStorage();
        foreach ($products as $product) {
            $this->addProduct($product);
        }
    }

    /**
     * @param \SplObjectStorage $objectStorage
     * @return self
     */
    private static function fromSplObjectStorage(\SplObjectStorage $objectStorage)
    {
        $pc = new self();
        $pc->products->addAll($objectStorage);

        return $pc;
    }

    /**
     * @param Product $product
     */
    public function addProduct(Product $product)
    {
        $this->products->attach($product);
    }

    /**
     * @param Product $product
     * @return self
     */
    public function removeProduct(Product $product)
    {
        $products = clone $this->products;
        $products->detach($product);

        return self::fromSplObjectStorage($products);
    }

    /**
     * @return float|int
     */
    public function calculatePricesSum()
    {
        $sum = 0;

        foreach ($this->products as $product) {
            $sum += $product->getPrice();
        }

        return $sum;
    }

    /**
     * @param self $productCollection
     * @return self
     */
    public function merge(self $productCollection)
    {
        $products = clone $this->products;
        $products->addAll($productCollection->products);

        return self::fromSplObjectStorage($products);
    }

    /**
     * @param self $productCollection
     * @return self
     */
    public function difference(self $productCollection)
    {
        $products = clone $this->products;
        $products->removeAll($productCollection->products);

        return self::fromSplObjectStorage($products);
    }

    /**
     * @param array $names
     * @return self
     */
    public function getAllExceptNames(array $names)
    {
        $allExceptNames = new \SplObjectStorage();

        foreach ($this->products as $product) {
            if (!in_array($product->getName(), $names, true)) {
                $allExceptNames->attach($product);
            }
        }

        return self::fromSplObjectStorage($allExceptNames);
    }

    /**
     * @param $name
     * @return Product|null
     */
    public function getFirstByName($name)
    {
        foreach ($this->products as $product) {
            if ($product->getName() === $name) {
                return $product;
            }
        }
        return null;
    }

    /**
     * @param array $names
     * @return Product|null
     */
    public function getFirstByNames(array $names)
    {
        foreach ($names as $name) {
            $product = $this->getFirstByName($name);
            if ($product) {
                return $product;
            }
        }
        return null;
    }

    /** * @return int
     */
    public function count()
    {
        return count($this->products);
    }

    /**
     * @return Product[]|\SplObjectStorage
     */
    public function getIterator()
    {
        return $this->products;
    }
}

