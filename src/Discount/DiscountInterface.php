<?php

namespace Discount;

use Collection\ProductCollection;
use DiscountResult;

interface DiscountInterface
{
    /**
     * @param ProductCollection $notUsedProducts
     * @return DiscountResult
     */
    public function applyDiscount(ProductCollection $notUsedProducts);
}