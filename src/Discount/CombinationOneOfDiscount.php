<?php

namespace Discount;

use Collection\ProductCollection;
use DiscountResult;

class CombinationOneOfDiscount implements DiscountInterface
{
    private $productName;
    private $oneOfNames;

    /**
     * @var float|int
     */
    private $percent;

    public function __construct($productName, array $oneOfNames, $percent)
    {
        $this->oneOfNames = $oneOfNames;
        $this->productName = $productName;
        $this->percent = $percent;
    }

    /**
     * @param ProductCollection $notUsedProducts
     * @return DiscountResult
     */
    public function applyDiscount(ProductCollection $notUsedProducts) {
        $product = $notUsedProducts->getFirstByName($this->productName);
        $firstByName = $notUsedProducts->getFirstByNames($this->oneOfNames);

        if ($product && $firstByName) {
            $matchedProducts = new ProductCollection([$product, $firstByName]);
            $originalSum = $matchedProducts->calculatePricesSum();
            return new DiscountResult($matchedProducts, $originalSum - $originalSum * $this->percent);
        }

        return DiscountResult::makeEmpty();
    }
}