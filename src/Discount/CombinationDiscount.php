<?php

namespace Discount;

use Collection\ProductCollection;
use DiscountResult;

class CombinationDiscount implements DiscountInterface
{
    /**
     * @var array
     */
    private $names;

    /**
     * @var float|int
     */
    private $percent;

    public function __construct(array $names, $percent)
    {
        $this->names = $names;
        $this->percent = $percent;
    }

    /**
     * @param ProductCollection $notUsedProducts
     * @return DiscountResult
     */
    public function applyDiscount(ProductCollection $notUsedProducts) {
        $productsUsedInThisDiscount = new ProductCollection();

        foreach ($this->names as $name) {
            $matchedProduct = $notUsedProducts->getFirstByName($name);
            if ($matchedProduct) {
                $notUsedProducts = $notUsedProducts->removeProduct($matchedProduct);
                $productsUsedInThisDiscount->addProduct($matchedProduct);
            } else {
                return DiscountResult::makeEmpty();
            }
        }

        $originalSum = $productsUsedInThisDiscount->calculatePricesSum();

        return new DiscountResult($productsUsedInThisDiscount, $originalSum - $originalSum * $this->percent);
    }
}