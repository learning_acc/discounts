<?php

namespace Discount;

use Collection\ProductCollection;
use DiscountResult;

class CountDiscount implements DiscountInterface
{
    /**
     * @var array
     */
    private $exceptNames;

    /**
     * @var array
     */
    private $countPercentMap;

    /**
     * @param array $countPercentMap Массив, содержащий соответствия между количеством продуктов и Depreciation
     * @param array $exceptNames
     */
    public function __construct(array $countPercentMap, array $exceptNames = [])
    {
        if (!$this->isCountPercentMapValid($countPercentMap)) {
            throw new \InvalidArgumentException("");
        }

        $this->exceptNames = $exceptNames;
        $this->countPercentMap = $countPercentMap;
    }

    /**
     * @param array $countPercentMap
     * @return bool
     */
    private function isCountPercentMapValid(array $countPercentMap)
    {
        foreach ($countPercentMap as $count => $percent) {
            if (!is_numeric($count) || !is_numeric($percent)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param ProductCollection $notUsedProducts
     * @return DiscountResult
     */
    public function applyDiscount(ProductCollection $notUsedProducts) {
        $matchedProducts = $notUsedProducts->getAllExceptNames($this->exceptNames);
        $countMatched = $matchedProducts->count();

        if (array_key_exists($countMatched, $this->countPercentMap)) {
            return $this->createDiscountResult($matchedProducts, $countMatched);
        }

        $maxCount = max(array_keys($this->countPercentMap));
        if ($countMatched > $maxCount) {
            return $this->createDiscountResult($matchedProducts, $maxCount);
        }

        return DiscountResult::makeEmpty();
    }

    /**
     * Вспомогательный метод для уменьшения дублирования кода
     *
     * @param ProductCollection $matchedProducts
     * @param mixed $keyOfMap Ключ в массиве $this->countPercentMap, значению которого соответствует подходящий Percent
     * @return DiscountResult
     */
    private function createDiscountResult(ProductCollection $matchedProducts, $keyOfMap)
    {
        $percent = $this->countPercentMap[$keyOfMap];
        $originalSum = $matchedProducts->calculatePricesSum();
        return new DiscountResult($matchedProducts, $originalSum - $originalSum * $percent);
    }
}