<?php

class Product
{
    /**
     * @var mixed
     */
    private $name;

    /**
     * @var float|int
     */
    private $price;

    /**
     * @param mixed $name
     * @param float|int $price
     */
    public function __construct($name, $price)
    {
        $this->name = $name;
        $this->price = $price;
    }

    /**
     * @return float|int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }
}