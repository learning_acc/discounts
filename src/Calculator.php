<?php

use Collection\DiscountCollection;
use Collection\ProductCollection;

class Calculator
{
    /**
     * @param DiscountCollection $discountCollection
     * @param ProductCollection $productCollection
     * @return int|float
     */
    public function calculateTotalPrice(
        DiscountCollection $discountCollection,
        ProductCollection $productCollection
    ) {
        $usedProducts = new ProductCollection();
        $totalPrice = 0;

        foreach ($discountCollection as $discount) {
            $notUsedProducts = $productCollection->difference($usedProducts);
            $discountResult = $discount->applyDiscount($notUsedProducts);
            $totalPrice += $discountResult->getPriceWithDiscount();
            $usedProducts = $usedProducts->merge($discountResult->getMatchedProducts());
        }

        $totalPrice += $productCollection->difference($usedProducts)->calculatePricesSum();

        return $totalPrice;
    }
}